
# coding: utf-8

# In[4]:


import bcolz
import tensorflow as tf
import numpy as np
import os
import time
import time

train = bcolz.open('Bcolz_train_data.dat', mode='r')
test = bcolz.open('Bcolz_test_data.dat', mode='r')
valid = bcolz.open('Bcolz_valid_data.dat', mode='a')

train_lengths = np.load(open('train_lengths.npy', 'rb'))
test_lengths = np.load(open('test_lengths.npy', 'rb'))
valid_lengths = np.load(open('valid_lengths.npy', 'rb'))
train_labels = np.load(open('labels/train_labels.npy','rb'))
valid_labels = np.load(open('labels/valid_labels.npy','rb'))

print(len(train))
print(len(test))
print(len(valid))
print(len(train_lengths))
print(len(test_lengths))
print(len(valid_lengths))
print(train_labels.shape)
print(valid_labels.shape)


display_step = 1000 ## How often to print results and save checkpoint.
sample_dataset_mean = np.mean(train[0:30000], axis = (0,1)) ## just for tonight while we don't want to redo our entire dataset
sample_dataset_variance = np.var(train[0:30000], axis = (0,1))



model_name = 'Overnight_6_layer_feedforward_v4'
batch_size = 16 
max_time_steps = 1800
epochs = 100
learning_rate = 0.0005
context = 5
device_name = "/gpu:0"



with tf.device(device_name):

    tf.reset_default_graph()
    graph = tf.Graph()

    logs_path = '/tmp/'+model_name+'/'+str(time.time())
    
    
    

    # Network Parameters
    n_input = 80 # 80 mel_filter_bank_coeff
    n_hidden = 1024 # hidden layer 
    n_projection = 512
    n_classes = 5761 
    

    

    # tf Graph input
    x = tf.placeholder(tf.float32, shape=(batch_size, 1+context*2,  n_input), name = 'x') ## this is [batch_size, n_steps, len_input_feature_vector]
    
    index_in_time = tf.placeholder(tf.int32, name = 'index_in_time')
    
    #softmax_weights = tf.placeholder(tf.int32)
    
    #sequence_lengths = tf.placeholder(tf.int32, shape=(batch_size), name = 'sequence_lengths') 
    
    dense_labels = tf.placeholder(tf.int32, shape=(batch_size, max_time_steps), name = 'dense_labels') 

    # time batch refers to batches through time used for truncated backprop.
    time_batch_dense_labels = tf.expand_dims(dense_labels[:, index_in_time-context], axis = -1)
    print(time_batch_dense_labels)
    ###################################################################################################
    #time_batch_x = x[:, index_in_time-context: index_in_time+context+1, n_input]
    time_batch_x_reshaped  = tf.reshape(x, [batch_size, (context+context+1)*n_input])
    print(time_batch_x_reshaped)

    # Define weights
    # our input is 80*21 long, 80 dimensional vectors, inc the timestep we are interested in and 10 for context on either side. 
    weights = {
        'h1': tf.Variable(tf.random_normal([n_input*(1+context*2), n_hidden])),
        'b1': tf.Variable(tf.random_normal([n_hidden])),
        'h2': tf.Variable(tf.random_normal([n_hidden, n_hidden])),
        'b2': tf.Variable(tf.random_normal([n_hidden])),
        'h3': tf.Variable(tf.random_normal([n_hidden, n_hidden])),
        'b3': tf.Variable(tf.random_normal([n_hidden])),
        'h4': tf.Variable(tf.random_normal([n_hidden, n_hidden])),
        'b4': tf.Variable(tf.random_normal([n_hidden])),
        'h5': tf.Variable(tf.random_normal([n_hidden, n_hidden])),
        'b5': tf.Variable(tf.random_normal([n_hidden])),
        'h6': tf.Variable(tf.random_normal([n_hidden, n_hidden])),
        'b6': tf.Variable(tf.random_normal([n_hidden])),
        'out': tf.Variable(tf.random_normal([n_hidden, n_classes])),
        'bout': tf.Variable(tf.random_normal([n_classes]))

    }

    

    
        # Create model
    def multilayer_perceptron(x):
        
        layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights['h1']), weights['b1']))
        layer_2 = tf.nn.relu(tf.add(tf.matmul(layer_1, weights['h2']), weights['b2']))
        layer_3 = tf.nn.relu(tf.add(tf.matmul(layer_2, weights['h3']), weights['b3']))
        layer_4 = tf.nn.relu(tf.add(tf.matmul(layer_2, weights['h4']), weights['b4']))
        layer_5 = tf.nn.relu(tf.add(tf.matmul(layer_2, weights['h5']), weights['b5']))
        layer_6 = tf.nn.relu(tf.add(tf.matmul(layer_2, weights['h6']), weights['b6']))
       
        out_layer = tf.nn.relu(tf.matmul(layer_6, weights['out']) +weights['bout'])
        return out_layer
    
    logits = multilayer_perceptron(time_batch_x_reshaped)
    print('logits', logits)
    to_softmax_logits = tf.reshape(logits, [batch_size, 1, n_classes])
    print('to_softmax_logits', to_softmax_logits)
    #loss = tf.reduce_mean(tf.nn.ctc_loss(targetY, logits_as_tensor, sequence_Lengths, time_major= True)) # time_major true saves us reshaping.
    
    ######### Sequence Style loss ######################
#     # make a mask which tells us what to consider in our softmax loss
#     softmax_weights = tf.cast(tf.sequence_mask(sequence_lengths, 1), tf.float32) 
    
#     print('Softmax Weights', softmax_weights.shape)
#     print('tbdl', time_batch_dense_labels)
#     loss = tf.contrib.seq2seq.sequence_loss(
#         to_softmax_logits,  # should be 1800, batch, 5761
#         time_batch_dense_labels, ## should be batch, seq_len
#         softmax_weights) ### should be batch, seq_len 

    one_hot_labels = tf.squeeze(tf.one_hot(time_batch_dense_labels, depth = 5761), axis = 1)
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=one_hot_labels))
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
    
    pred = tf.nn.softmax(to_softmax_logits)
    pred_idx = tf.to_int32(tf.argmax(pred, -1))

    
    # print(time_batch_dense_labels.shape)
    
    # print(pred_idx.shape)
    
         
    accuracy = tf.reduce_mean(tf.cast(tf.equal(pred_idx, time_batch_dense_labels), tf.float32), name='acc')
    
    #error = tf.reduce_sum(tf.edit_distance(predictions, targetY, normalize=True))
    # Initializing the variables
    init = tf.global_variables_initializer()


################################################################################################################################################################################################
################################################################################################################################################################################################################################

sess = tf.InteractiveSession()
saver = tf.train.Saver()  

if os.path.exists(os.getcwd()+ '/saved_models/'+model_name+'/'):
    print("Restoring_Variables")
    saver.restore(sess, os.getcwd()+ '/saved_models/'+model_name+'/' + model_name)
else:
    print('Init fresh model.')
    sess.run(init)

print('initialised')
summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())


# In[67]:


################################################################################################################################################################################################
################################################################################################################################################################################################

current_epochs = 0
# Launch the graph

print('launching')
# with tf.Session(config=tf.ConfigProto(log_device_placement=True)) as sess:
#     saver = tf.train.Saver()
#     sess.run(init)
#     print('initialised')
#     summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
step = 0
print(step, train.shape[0],batch_size )

while step < train.shape[0]/batch_size -1:

    rand_ind_train = np.random.randint(low = 0, high = (len(train)//batch_size - 1 ))
    print(step)
    batch_x = (train[rand_ind_train*batch_size:(rand_ind_train+1)*batch_size] - sample_dataset_mean)/sample_dataset_variance
    context_padding = np.zeros((batch_size, context,  n_input))

    batch_x = np.concatenate((batch_x, context_padding), axis = 1)
    batch_x = np.concatenate((context_padding, batch_x), axis = 1) # add context padding to either side. 

    batch_labels = train_labels[rand_ind_train*batch_size:(rand_ind_train+1)*batch_size]
    #print(batch_x.shape)
    
    #batch_sequence_lengths = train_lengths[rand_ind_train*batch_size:(rand_ind_train+1)*batch_size].astype(float)
    
    
    train_losses = []
    train_accs = []
    start = time.time()
    
    for time_ind in range(0, 30): # sample 30 timesteps from this batch.
        rand_time_ind = np.random.randint(low = context, high = (max_time_steps+context) - 1)
        
        x_batched_in_time = batch_x[:,rand_time_ind-context:rand_time_ind+1+context,:]
        
        #batch_sequence_lengths -= (np.ones(16)*(batches_thr_time_len)).astype(float) only needed if doing sequence style
        feedDict = {x: x_batched_in_time, dense_labels: batch_labels, index_in_time: rand_time_ind}
        
        _, l_, acc = sess.run([optimizer, loss, accuracy], feed_dict=feedDict)
        #print(l_)
        train_losses.append(l_)
        train_accs.append(acc)
        
    l_ = sum(train_losses)/len(train_losses)
    acc = sum(train_accs)/len(train_accs)
    print("Train Accuracy", acc, "Train Loss", l_, 'Batch time :', time.time() - start, 'Expected Epoch Time :', (time.time() - start)*6000)
    
    summary = tf.Summary()
    summary.value.add(tag="Train Accuracy", simple_value=acc)
    summary.value.add(tag="Train Loss", simple_value=l_)
    summary_writer.add_summary(summary, step*batch_size*30 + current_epochs * (len(train)) *30 ) #30 being number of samples.
    
    #######################################################################################################################
    if step % display_step == 0 and step > 0:
        print("Display Step")
        # Calculate batch accuracy
        
        # calculate validation-set accuracy, because the GPU RAM is too small for such a large batch (the entire validation set, feed it through bit by bit)
        accuracies = []
        
        rand_ind_range = 20

        rand_ind = np.random.randint(low = 0, high = (len(valid)//batch_size - rand_ind_range ))
        print('RandInt',rand_ind)
        
        for valid_step in range(rand_ind,rand_ind+rand_ind_range):
            valid_batch_data = (valid[valid_step*batch_size:(valid_step+1)*batch_size] - sample_dataset_mean) / sample_dataset_variance
            
            context_padding = np.zeros((batch_size, context,  n_input))

            valid_batch_data  = np.concatenate((valid_batch_data , context_padding), axis = 1)
            valid_batch_data  = np.concatenate((context_padding, valid_batch_data) , axis = 1) # add context padding to either side. 
    
            valid_batch_lengths = valid_lengths[valid_step*batch_size:(valid_step+1)*batch_size].astype(float)
            valid_batch_label = valid_labels[valid_step*batch_size:(valid_step+1)*batch_size]
            
            
            
            for time_ind in range(context, (max_time_steps+context)):
                x_batched_in_time = valid_batch_data[:,time_ind-context:time_ind+1+context,:]
                #valid_batch_lengths -= (np.ones(16)*(batches_thr_time_len)).astype(float)
                feedDict = {x: x_batched_in_time, dense_labels: valid_batch_label, index_in_time: time_ind}
                
            
                acc = sess.run(accuracy, feed_dict= feedDict)    
                accuracies.append(acc)
            

        # Create a new Summary object with your measure and add the summary
        summary = tf.Summary()
        summary.value.add(tag="Validation_Accuracy", simple_value=sum(accuracies)/len(accuracies))
        summary_writer.add_summary(summary, step*batch_size*30 + current_epochs * (len(train)) *30 )

        print('Validation_accuracy - ', acc)
        # Save a checkpoint
        if not os.path.exists(os.getcwd() + '/saved_models/' + model_name):
            os.makedirs(os.getcwd() + '/saved_models/' + model_name)
        saver.save(sess,  os.getcwd()+'/saved_models/' + model_name +'/'+ model_name)
    ########################################################################################################################################
        
        
    step += 1
    if current_epochs < epochs:
        if step >= (len(train)/batch_size) -1:
            print("###################### New epoch ##########")
            current_epochs = current_epochs + 1
            learning_rate = learning_rate- (learning_rate*0.15)
            step = 0