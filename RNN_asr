import bcolz
import tensorflow as tf
import numpy as np
import os
import time
import time

train = bcolz.open('Bcolz_train_data.dat', mode='r')
test = bcolz.open('Bcolz_test_data.dat', mode='r')
valid = bcolz.open('Bcolz_valid_data.dat', mode='a')
print(len(train))
print(len(test))
print(len(valid))
train_lengths = np.load(open('train_lengths.npy', 'rb'))
test_lengths = np.load(open('test_lengths.npy', 'rb'))
valid_lengths = np.load(open('valid_lengths.npy', 'rb'))
print(len(train_lengths))
print(len(test_lengths))
print(len(valid_lengths))

train_labels = np.load(open('labels/train_labels.npy','rb'))
valid_labels = np.load(open('labels/valid_labels.npy','rb'))

display_step = 100



model_name = 'Overnight_3_layer'
batch_size = 16 
max_time_steps = 1800
batches_thr_time_len = 20 # batches through the sequence to properly truncate backprop!
epochs = 100
learning_rate = 0.0005



device_name = "/gpu:0"

with tf.device(device_name):

    tf.reset_default_graph()
    graph = tf.Graph()

    logs_path = '/tmp/'+model_name+'/'+str(time.time())
    
    ## How often to print results and save checkpoint.
    

    # Network Parameters
    n_input = 80 # 80 mel_filter_bank_coeff
    n_hidden = 1024 # hidden layer numfeatures
    n_projection = 512
    n_classes = 5761 
    

    

    # tf Graph input
    x = tf.placeholder(tf.float32, shape=(batch_size, max_time_steps,  n_input)) ## this is [batch_size, n_steps, len_input_feature_vector]
    
    # Sequence length which lets the dynamic RNN know when to cut each X off. 
    sequence_Lengths = tf.placeholder(tf.int32, shape=(batch_size)) 
    
    index_in_time = tf.placeholder(tf.int32)
    
    softmax_weights = tf.placeholder(tf.int32)
    
    dense_labels = tf.placeholder(tf.int32, shape=(batch_size, max_time_steps)) 

    # time batch refers to batches through time used for truncated backprop.
    time_batch_dense_labels = dense_labels[:, index_in_time*batches_thr_time_len: (index_in_time+1)*batches_thr_time_len]
    ###################################################################################################

    rnn_layers = [tf.nn.rnn_cell.LSTMCell(size,state_is_tuple=True) for size in [n_hidden, n_hidden, n_hidden]]
    multi_rnn_cell = tf.nn.rnn_cell.MultiRNNCell(rnn_layers)
    state = multi_rnn_cell.zero_state(batch_size, tf.float32)
    
    # Define weights
    weights_512 = {
        'out': tf.Variable(tf.random_normal([n_hidden, n_projection]))
    }
    biases_512= {
        'out': tf.Variable(tf.random_normal([n_projection]))
    }
    
    weights_proj = {
        'out': tf.Variable(tf.random_normal([n_projection, n_classes]))
    }
    biases_proj = {
        'out': tf.Variable(tf.random_normal([n_classes]))
    }
    
    
     # call this when starting. 
    
    def RNN(x, in_state):

        print(x.shape)
        
        out, out_state = tf.nn.dynamic_rnn(multi_rnn_cell,x, initial_state = state, sequence_length = sequence_Lengths, swap_memory = True) # swap_memory = True is iimportant, otherwise gpu will probably run out of RAM
        
        
        return out, out_state
    
    output, next_state = RNN(x[:,index_in_time*batches_thr_time_len: (index_in_time+1)*batches_thr_time_len, : ], state)
    
    print(output.shape)
    # you have to split the outputs into a list along the time dimension.
    output_list = tf.split(output, batches_thr_time_len, 1)
    # reshape from [batch, 1, n_hidden] to [batch, n_hidden]
    output_list_multipliable = [tf.reshape(t, [batch_size, n_hidden]) for t in output_list]
    
    logits = [tf.matmul(  tf.nn.relu((tf.matmul(out, weights_512['out']) +  biases_512['out'])), weights_proj['out']) + biases_proj['out'] for out in output_list_multipliable] # its annoying the matrix multiplication doesn't broadcast in tensorflow.
    # from a list to [time, batch, output_features]
    logits_as_tensor = tf.stack(logits)
    print(logits_as_tensor.shape)
    
    to_softmax_logits = tf.reshape(logits_as_tensor, [batch_size, batches_thr_time_len, n_classes])
    #loss = tf.reduce_mean(tf.nn.ctc_loss(targetY, logits_as_tensor, sequence_Lengths, time_major= True)) # time_major true saves us reshaping.
    
    # make a mask which tells us what to consider in our softmax loss
    print(sequence_Lengths.shape)
    softmax_weights = tf.cast(tf.sequence_mask(sequence_Lengths, batches_thr_time_len), tf.float32) 
    
    #print('Softmax Weights', softmax_weights.shape)
    
    loss = tf.contrib.seq2seq.sequence_loss(
        to_softmax_logits,  # should be 1800, batch, 5761
        time_batch_dense_labels, ## should be batch, seq_len
        softmax_weights) ### should be batch, seq_len 
    
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
    
    pred = tf.nn.softmax(to_softmax_logits)
    
    
    pred_idx = tf.to_int32(tf.argmax(pred, -1))
    print(time_batch_dense_labels.shape)
    
    print(pred_idx.shape)
    
         
    accuracy = tf.reduce_mean(tf.cast(tf.equal(pred_idx, time_batch_dense_labels), tf.float32), name='acc')
    
    #error = tf.reduce_sum(tf.edit_distance(predictions, targetY, normalize=True))
    # Initializing the variables
    init = tf.global_variables_initializer()


################################################################################################################################################################################################
################################################################################################################################################################################################################################

sess = tf.InteractiveSession()
saver = tf.train.Saver()  

if os.path.exists(os.getcwd()+ '/saved_models/'+model_name+'/'):
    print("Restoring_Variables")
    saver.restore(sess, os.getcwd()+ '/saved_models/'+model_name+'/' + model_name)
else:
    print('Init fresh model.')
    sess.run(init)

print('initialised')
summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

################################################################################################################################################################################################
################################################################################################################################################################################################

current_epochs = 0
# Launch the graph

print('launching')
# with tf.Session(config=tf.ConfigProto(log_device_placement=True)) as sess:
#     saver = tf.train.Saver()
#     sess.run(init)
#     print('initialised')
#     summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
step = 0
print(step, train.shape[0],batch_size )

while step < train.shape[0]/batch_size:
    print(step)
    batch_x = train[step*batch_size:(step+1)*batch_size]
    batch_labels = train_labels[step*batch_size:(step+1)*batch_size]
    print(batch_x.shape)
    
    batch_sequence_lengths = train_lengths[step*batch_size:(step+1)*batch_size].astype(float)
    
    
    
    
    np_state = sess.run(state)
    
    
    
    train_losses = []
    train_accs = []
    start = time.time()
    for time_ind in range(0, (max_time_steps//batches_thr_time_len)):
        # batches through time is truncated back prop.
        batch_sequence_lengths -= (np.ones(16)*(batches_thr_time_len)).astype(float)# so that dynamicRNN outputs are automatically zerod when past their length, and these zeros don't bias our training. 
        feedDict = {x: batch_x, dense_labels: batch_labels, state: np_state, index_in_time: time_ind, sequence_Lengths: batch_sequence_lengths}
    
        _, l_, acc = sess.run([optimizer, loss, accuracy], feed_dict=feedDict)
        train_losses.append(l_)
        train_accs.append(acc)
        
    l_ = sum(train_losses)/len(train_losses)
    acc = sum(train_accs)/len(train_accs)
    print("Train Accuracy", acc, "Train Loss", l_, 'Batch time :', time.time() - start, 'Expected Epoch Time :', (time.time() - start)*6000)
    
    summary = tf.Summary()
    summary.value.add(tag="Train Accuracy", simple_value=acc)
    summary.value.add(tag="Train Loss", simple_value=l_)
    summary_writer.add_summary(summary, step*batch_size+current_epochs * (len(train)))
    
    #######################################################################################################################
    if step % display_step == 0 and step > 0:
        print("Display Step")
        # Calculate batch accuracy
        
        # calculate validation-set accuracy, because the GPU RAM is too small for such a large batch (the entire validation set, feed it through bit by bit)
        accuracies = []
        
        rand_ind = np.random.randint(low = 0, high = (len(valid)//batch_size - 10))
        
        
        for valid_step in range(rand_ind,rand_ind+30):
            valid_batch_data = valid[valid_step*batch_size:(valid_step+1)*batch_size]
            valid_batch_lengths = valid_lengths[valid_step*batch_size:(valid_step+1)*batch_size].astype(float)
            valid_batch_label = valid_labels[valid_step*batch_size:(valid_step+1)*batch_size]
            
            np_state = sess.run(state)
            
            for time_ind in range(0, (max_time_steps//batches_thr_time_len)):
                valid_batch_lengths -= (np.ones(16)*(batches_thr_time_len)).astype(float)
                feedDict = {x: valid_batch_data, dense_labels: valid_batch_label, state:np_state, index_in_time: time_ind, sequence_Lengths: valid_batch_lengths}
                
            
                acc = sess.run(accuracy, feed_dict= feedDict)    
                accuracies.append(acc)
            

        # Create a new Summary object with your measure and add the summary
        summary = tf.Summary()
        summary.value.add(tag="Validation_Accuracy", simple_value=sum(accuracies)/len(accuracies))
        summary_writer.add_summary(summary, step*batch_size +current_epochs * (len(train)))
        # Save a checkpoint
        if not os.path.exists(os.getcwd() + '/saved_models/' + model_name):
            os.makedirs(os.getcwd() + '/saved_models/' + model_name)
        saver.save(sess,  os.getcwd()+'/saved_models/' + model_name +'/'+ model_name)
    ########################################################################################################################################
        
        
    step += 1
    if current_epochs < epochs:
        if step >= (len(train)/batch_size):
            print("###################### New epoch ##########")
            current_epochs = current_epochs + 1
            learning_rate = learning_rate- (learning_rate*0.15)
            step = 0